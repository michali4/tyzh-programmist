import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AuthLoyoutComponent } from './components/loyouts/auth-loyout/auth-loyout.component';
import { SiteLoyoutComponent } from './components/loyouts/site-loyout/site-loyout.component';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { RegisterPageComponent } from './components/register-page/register-page.component';
import { AboutPageComponent } from './components/about-page/about-page.component';
import { ServicesPageComponent } from './components/services-page/services-page.component';
import { SpecialistsPageComponent } from './components/specialists-page/specialists-page.component';
import { OrderPageComponent } from './components/order-page/order-page.component';
import { OverviewPageComponent } from './components/overview-page/overview-page.component';
import { LoaderComponent } from './components/loader/loader.component';
import { TokenInterceptor } from './shared/classes/token.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    AuthLoyoutComponent,
    SiteLoyoutComponent,
    LoginPageComponent,
    RegisterPageComponent,
    AboutPageComponent,
    ServicesPageComponent,
    SpecialistsPageComponent,
    OrderPageComponent,
    OverviewPageComponent,
    LoaderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      multi: true,
      useClass:TokenInterceptor
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
