import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginPageComponent } from './components/login-page/login-page.component';
import { RegisterPageComponent } from './components/register-page/register-page.component';
import { AuthLoyoutComponent } from './components/loyouts/auth-loyout/auth-loyout.component';
import { SiteLoyoutComponent } from './components/loyouts/site-loyout/site-loyout.component';
import { AboutPageComponent } from './components/about-page/about-page.component';
import { AuthGuard } from './shared/classes/auth.guard';
import { OverviewPageComponent } from './components/overview-page/overview-page.component';
import { SpecialistsPageComponent } from './components/specialists-page/specialists-page.component';
import { ServicesPageComponent } from './components/services-page/services-page.component';
import { OrderPageComponent } from './components/order-page/order-page.component';

const routes: Routes = [
  {
    path: '', component: AuthLoyoutComponent, children: [
      { path: '', redirectTo: '/about', pathMatch: 'full' },
      { path: 'about', component: AboutPageComponent },
      { path: 'login', component: LoginPageComponent },
      { path: 'register', component: RegisterPageComponent }
    ]
  },
  {
    path: '', component: SiteLoyoutComponent, canActivate: [AuthGuard], children: [
      { path: 'overview', component: OverviewPageComponent },
      { path: 'specialists', component: SpecialistsPageComponent },
      { path: 'services', component: ServicesPageComponent },
      { path: 'order', component: OrderPageComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
