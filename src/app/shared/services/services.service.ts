import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { URLS } from 'src/app/config';
import { Service, Message } from '../interfaces';


@Injectable({
  providedIn: 'root'
})
export class ServicesService {
  constructor(private http: HttpClient) { }

  fetch(): Observable<Service[]> {
    return this.http.get<Service[]>(`${URLS.urlAPI}/api/services`);
  }

  create(service: Service): Observable<Service> {
    return this.http.post<Service> (`${URLS.urlAPI}/api/services`,service);
  }

  update(service: Service): Observable<Service> {
    return this.http.patch<Service> (`${URLS.urlAPI}/api/services/${service._id}`,service);
  }

  delete(id: string): Observable<Message> {
    return this.http.delete<Message>(`${URLS.urlAPI}/api/services/${id}`);
  }
}