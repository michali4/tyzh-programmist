import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { User } from '../interfaces';
import {URLS} from '../../config';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private token = null;

  constructor(private http: HttpClient) {

  }
  register(user: User): Observable<User> {
    // console.log(`${URLS.urlAPI}/api/auth/register`);
    return this.http.post<User>(`${URLS.urlAPI}/api/auth/register`, user);
  }

  login(user: User): Observable<{ token: string }> {
    // console.log(`${URLS.urlAPI}/api/auth/login`);
    return this.http.post<{ token: string }>(`${URLS.urlAPI}/api/auth/login`, user)
      .pipe(
        tap(({token}) => {
          localStorage.setItem('auth-token', token);
          this.setToken(token);
        })
      );
  }

  setToken(token: string) {
    this.token = token;
  }

  getToken(): string {
    return this.token;
  }

  isAuthenticated(): boolean {
    return !!this.token;
  }

  logout() {
    this.setToken(null);
    localStorage.clear();
  }
}
