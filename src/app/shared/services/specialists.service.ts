import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { URLS } from 'src/app/config';
import { Specialist, Message } from '../interfaces';


@Injectable({
  providedIn: 'root'
})

export class SpecialistsService {
  constructor(private http: HttpClient) { }

  fetch(): Observable<Specialist[]> {
    return this.http.get<Specialist[]>(`${URLS.urlAPI}/api/specialists`);
  }

  create(specialist: Specialist): Observable<Specialist> {
    return this.http.post<Specialist> (`${URLS.urlAPI}/api/specialists`,specialist);
  }

  update(specialist: Specialist): Observable<Specialist> {
    return this.http.patch<Specialist> (`${URLS.urlAPI}/api/specialists/${specialist._id}`,specialist);
  }

  delete(id: string): Observable<Message> {
    return this.http.delete<Message>(`${URLS.urlAPI}/api/specialists/${id}`);
  }
}
