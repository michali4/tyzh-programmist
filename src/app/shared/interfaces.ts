export interface User {
    email: string;
    password: string;
    name: string;
}

export interface Specialist {
    _id?: string;
    name: string;
    nickName: string;
    user?: string;
    photoSrc?: string;
    shortDescript?: string;
    competencies?: string[];//string[]массив строковый, содержит перечень навыков специалиста
    rating?: number; //число, либо массив чисел всех оценок специалиста

}

export interface Service {
    _id?: string;
    name: string;

    // amountExecutions?: number;//количество раз оказания этой услуги
    // numberOfSpecialists?: number;//количество специалистов, обладающих этим навыком
    // orderTime?: string; //это поле должно содержать массив с временем оказания этой услуги

}

export interface Order {
    _id?: string;
    date?: string; //TODO тип заглушка. в дальнейшем установить нужный
    order: number;
    list?: string; //TODO тип заглушка. в дальнейшем установить нужный
    user?: string;
}

export interface Message {
    message: string
}