import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecialistsPageComponent } from './specialists-page.component';

describe('SpecialistsPageComponent', () => {
  let component: SpecialistsPageComponent;
  let fixture: ComponentFixture<SpecialistsPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpecialistsPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecialistsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
