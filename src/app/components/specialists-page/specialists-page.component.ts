import { Component, OnInit, AfterViewInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';

import { Specialist } from 'src/app/shared/interfaces';
import { MaterialInstance, MaterialService } from 'src/app/shared/classes/material.service';
import { SpecialistsService } from 'src/app/shared/services/specialists.service';


@Component({
  selector: 'app-specialists-page',
  templateUrl: './specialists-page.component.html',
  styleUrls: ['./specialists-page.component.css']
})
export class SpecialistsPageComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('modal') modalRef: ElementRef;
  modal: MaterialInstance;
  form: FormGroup;

  // loading = false;
  // specialists: Specialist []= [];
  specialists$: Observable<Specialist[]>;
  modalTitle: string;

  constructor(private specialistsService: SpecialistsService) { }

  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl(null, Validators.required),
      nickName: new FormControl(null, Validators.required),
      photoSrc: new FormControl(null, null),
      shortDescript: new FormControl(null, null),
      competencies: new FormControl(null, null)
    });
    // this.loading = true;
    // this.specialistsService.fetch().subscribe(specialists => {
    //   this.loading = false;
    //   this.specialists = specialists;
    //   console.log('specialists',specialists);
    // })
    this.specialists$ = this.specialistsService.fetch();
  }

  ngOnDestroy() {
    this.modal.destroy();
  }

  ngAfterViewInit() {
    this.modal = MaterialService.initModal(this.modalRef);
  }

  onAddSpecialist() {
    this.modalTitle = 'Добавить специалиста';
    this.modal.open();
  }

  onDeleteSpecialist(event: Event, specialist: Specialist) {
    event.stopPropagation();
    const decision = window.confirm(`Удалить специалиста "${specialist.name}"?`)
    //TODO доделать удаление
  }

  onSelectSpecialist(specialist: Specialist) {
    this.modalTitle = 'Изменить данные специалиста';
    this.form.patchValue({
      name: specialist.name,
      nickName: specialist.nickName,
      photoSrc: specialist.photoSrc,
      shortDescript: specialist.shortDescript,
      competencies: specialist.competencies,
    });
    this.modal.open();
    MaterialService.updateTextInputs();
  }

  onCancel() {
    this.modal.close();
    this.form.reset({
      name: '',
      nickName: '',
      photoSrc: '',
      shortDescript: '',
      competencies: [],
    });
    MaterialService.updateTextInputs();
  }

  onSubmit () {
    this.form.disable();
    const newSpecialist: Specialist = {
      name: this.form.value.name,
      nickName: this.form.value.nickName,
      shortDescript: this.form.value.shortDescript
    }
    this.specialistsService.create(newSpecialist).subscribe(
      specialist => {
        MaterialService.toast('Специалист добавлен');
        // this.specialists.push(specialist)
      }, error => {
        MaterialService.toast(error.error.message);
      }, () => {
        this.modal.close();
        this.form.reset({
          name: '',
          nickName: '',
          photoSrc: '',
          shortDescript: '',
          competencies: '',
        });
        this.form.enable();
      }
    )

  }
}
