import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
  selector: 'app-site-loyout',
  templateUrl: './site-loyout.component.html',
  styleUrls: ['./site-loyout.component.css']
})
export class SiteLoyoutComponent implements OnInit {

  links = [
    // {url: '/overview', name: 'Обзор'},
    {url: '/order', name: 'Заказать Услугу'},
    {url: '/services', name: 'Доступные Услуги'}, 
    {url: '/specialists', name: 'Наши Специалисты'},
    {url: '/about', name: 'О нас'}
  ]
  constructor(private auth: AuthService, private router: Router) { }

  ngOnInit() {
  }

  logout(event: Event) {
    event.preventDefault();
    this.auth.logout();
    this.router.navigate(['/login']);
  }
}
