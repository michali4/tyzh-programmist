import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiteLoyoutComponent } from './site-loyout.component';

describe('SiteLoyoutComponent', () => {
  let component: SiteLoyoutComponent;
  let fixture: ComponentFixture<SiteLoyoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiteLoyoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteLoyoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
