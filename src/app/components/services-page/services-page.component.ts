import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { Service } from 'src/app/shared/interfaces';
import { ServicesService } from 'src/app/shared/services/services.service';
import { MaterialInstance, MaterialService } from 'src/app/shared/classes/material.service';



@Component({
  selector: 'app-services-page',
  templateUrl: './services-page.component.html',
  styleUrls: ['./services-page.component.css']
})
export class ServicesPageComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('modal') modalRef: ElementRef;
  modal: MaterialInstance;
  form: FormGroup;
  modalTitle: string;
  services$: Observable<Service[]>;

  constructor(private servicesService: ServicesService) { }

  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl(null, Validators.required),
    });
    this.services$ = this.servicesService.fetch();
  }

  ngOnDestroy() {
    this.modal.destroy();
  }

  ngAfterViewInit() {
    this.modal = MaterialService.initModal(this.modalRef);
  }

  onAddService() {
    this.modalTitle = 'Добавить услугу';
    this.modal.open();
  }

  onDeleteService(event: Event, service: Service) {
    event.stopPropagation();
    const decision = window.confirm(`Удалить услугу "${service.name}"?`)
    //TODO доделать удаление
    if (decision) {
      this.servicesService.delete(service._id)
        .subscribe(
          response => MaterialService.toast(response.message),
          error => MaterialService.toast(error.error.message),
          () => this.services$ = this.servicesService.fetch()
        )
    }
  }

  onSelectService(service: Service) {
    this.modalTitle = 'Скорректировать название услуги';
    this.form.patchValue({
      name: service.name
    });
    this.modal.open();
    MaterialService.updateTextInputs();
  }

  onCancel() {
    this.modal.close();
    this.form.reset({
      name: ''
    });
    MaterialService.updateTextInputs();
  }

  closeModalWindow = () => {
    this.modal.close();
    this.form.reset({
      name: ''
    });
    this.form.enable();
  }

  onSubmit() {
    this.form.disable();
    const newService: Service = {
      name: this.form.value.name
    }
    this.servicesService.create(newService).subscribe(
      specialist => {
        MaterialService.toast('Услуга добавлена');
        this.services$ = this.servicesService.fetch(); //TODO добавлено для обновления списка с услугами. в дальнейшем надо переделать это. убрать этот запрос списка
      }, error => {
        MaterialService.toast(error.error.message);
        this.closeModalWindow();
      }, () => this.closeModalWindow()
    )
  }
}
