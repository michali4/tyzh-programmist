import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Subscription } from 'rxjs';

import { AuthService } from 'src/app/shared/services/auth.service';
import { MaterialService } from 'src/app/shared/classes/material.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit, OnDestroy {

  form: FormGroup;
  aSub: Subscription;

  constructor(private auth: AuthService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.form = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [Validators.required, Validators.minLength(6)])
    });
    this.route.queryParams.subscribe((params: Params) => {
      if (params['registered']) {
        MaterialService.toast('Теперь вы можете зайти в систему используя свои данные');
        // tslint:disable-next-line:comment-format
        //Теперь вы можете зайти в систему используя свои данные
      } else if (params['accessDenied']) {
        MaterialService.toast('Для начала авторизуйтесь в системе ');
        // для начала авторизуйтесь в системе
      } else if (params['sessionFailed']) {
        MaterialService.toast('Пожалуйста войдите в систему заново')
      }
    });
  }
  ngOnDestroy() {
    if (this.aSub) {
      this.aSub.unsubscribe();
    }
  }

  onSubmit() {
    this.form.disable();
    // const user = {
    //   email: this.form.value.email,
    //   password: this.form.value.password

    // };
    // this.auth.login(user);
    this.aSub = this.auth.login(this.form.value).subscribe(
      () => {
        console.log('Login success');
        this.router.navigate(['/services']);
      },
      error => {
        MaterialService.toast(error.error.message);
        // console.warn(error);
        this.form.enable();
      }
    );
  }
}
